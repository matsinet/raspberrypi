import RPi.GPIO as gpio
import time

# Pin Definitions
relayPin = 4
switchPin = 21
ledPin = 13
reedPin = 19

# Set the pin mode
gpio.setmode(gpio.BCM)

# GPIO setup
gpio.setup(relayPin, gpio.OUT)
gpio.setup(ledPin, gpio.OUT)
gpio.setup(switchPin, gpio.IN, gpio.PUD_UP)
gpio.setup(reedPin, gpio.IN, gpio.PUD_UP)

print("Turn the relay on")
gpio.output(relayPin, gpio.LOW)
time.sleep(5)
gpio.output(relayPin, gpio.HIGH)

print("Read the reed switch status") 
print(gpio.input(reedPin))

print("Read the switch status")
print(gpio.input(switchPin))

print("Turn on the LED")
switch = gpio.output(ledPin, gpio.HIGH)
time.sleep(1)
switch = gpio.output(ledPin, gpio.LOW)

# Clean up this mess
gpio.cleanup()

# Import the GPIO and time modules
import RPi.GPIO as gpio
import time

# Define GPIO Pins Locations
relayPin = 4
switchPin = 21
ledPin = 13
reedPin = 19

# Set up some house keeping variables
pauseTime = 5
switchOn = 0

# Set the pin mode
# BCM mode references the pin numbers on the chip
# vs the layout of the board
gpio.setmode(gpio.BCM)

# GPIO setup
gpio.setup(relayPin, gpio.OUT)
gpio.setup(ledPin, gpio.OUT)
gpio.setup(switchPin, gpio.IN, gpio.PUD_UP)
gpio.setup(reedPin, gpio.IN, gpio.PUD_UP)

# Set the relay pin to be high by default
gpio.output(relayPin, gpio.HIGH)
try:
    while 1:
        # Turn on the relay if the button is pressed
        if switchOn == 0 and not gpio.input(switchPin):
            print("Operating the door")
            switchOn = time.time()
            gpio.output(relayPin, gpio.LOW)

        # Turn off the relay after the pauseTime
        if switchOn != 0 and (time.time() - switchOn) > pauseTime:
            switchOn = 0
            gpio.output(relayPin, gpio.HIGH)

        # Turn on the LED if the reed switch is open
        if gpio.input(reedPin):
            gpio.output(ledPin, gpio.HIGH)
        # Turn off the LED if the reed switch is closed
        else:
            gpio.output(ledPin, gpio.LOW)

        # Sleep for the quarter second
        time.sleep(.25)
except KeyboardInterrupt:
    # Shutdown cleanly if Ctrl-C is pressed
    print("Cleaning things up")
    gpio.cleanup()